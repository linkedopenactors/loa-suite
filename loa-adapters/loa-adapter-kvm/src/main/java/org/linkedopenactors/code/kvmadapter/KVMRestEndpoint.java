package org.linkedopenactors.code.kvmadapter;

import java.time.Instant;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

@Component
@Slf4j
public class KVMRestEndpoint {

	@Value("${app.kvmUrl}")
	private String kvmUrl;

	@Value("${app.kvmAdminUserName}")
	private String kvmAdminUserName;

	@Value("${app.kvmAdminPassword}")
	private String kvmAdminPassword;

	private WebClient webClient;

	public KVMRestEndpoint() {
		this.webClient = WebClient.builder().build();
	}
	
	public Flux<KvmEntry> getChangedEntriesSince(Instant since) {
		Instant until = Instant.now();
		log.debug("getting changes between " + since + " and " + until );
		long sinceAsUnixTimestamp = since.getEpochSecond();
		long untilAsUnixTimestamp = until.getEpochSecond();

		String url = kvmUrl + "entries/recently-changed?since="+sinceAsUnixTimestamp+"&until="+untilAsUnixTimestamp+"&with_ratings=false&limit=1000&offset=0";
		Flux<KvmEntry> response = webClient
				.get().uri(url)
				.retrieve()
				.bodyToFlux(KvmEntry.class);

		return response;
	}
	
	public Flux<KvmEntry> findByBoundingBox(Double latleftTop, Double lngleftTop, Double latRightBottom,
			Double lngRightBottom) {

		String url = kvmUrl + "search?bbox="+latleftTop+","+lngleftTop+","+latRightBottom+","+lngRightBottom+"&status=created,confirmed";
		log.trace("kvmUrl: " + url);

		return webClient.get().uri(url).retrieve().bodyToFlux(KvmSearchResult.class).flatMap(result->Flux.fromIterable(result.getVisible()));
	}
	
	public String getToken(String userName, String pwd) {
		String url = kvmUrl + "/login";

		String body = "{\n" + "  \"email\": \"" + userName + "\",\n" + "  \"password\": \"" + pwd + "\"\n" + "}";
		return webClient.post()
				.uri(url)
				.header("accept", "application/json")
				.header("Content-Type", "application/json")
				.body(BodyInserters.fromValue(body))
				.retrieve()
				.bodyToMono(Map.class)
				.block()
				.get("token")
				.toString();
	}
	
	public String getPlacesHistory(String id) {
		String token = getToken(kvmAdminUserName, kvmAdminPassword);		
		String url = kvmUrl + "places/" + id + "/history/";
		return webClient.get()
			.uri(url)
			.header("Authorization", "Bearer " + token)
			.retrieve()
			.bodyToMono(String.class)
			.block();
	}
}
