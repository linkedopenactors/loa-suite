package org.linkedopenactors.code.kvmadapter;

import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.linkedopenactors.code.comparator.ComparatorModel;
import org.linkedopenactors.code.similaritychecker.BoundingBox;
import org.linkedopenactors.code.similaritychecker.SimilarityCheckerLoaAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import reactor.core.publisher.Flux;

@Component
public class KvmLoaAdapter implements SimilarityCheckerLoaAdapter {
	
	private KVMRestEndpoint kvmRestEndpoint;
	private KvmEntry2PublicationComparatorModel kvmEntry2PublicationComparatorModel;
	private String baseNamespace;

	public KvmLoaAdapter(KVMRestEndpoint kvmRestEndpoint, KvmEntry2PublicationComparatorModel kvmEntry2PublicationComparatorModel, @Value("${app.baseNamespace}") String baseNamespace) {
		this.kvmRestEndpoint = kvmRestEndpoint;
		this.kvmEntry2PublicationComparatorModel = kvmEntry2PublicationComparatorModel;
		this.baseNamespace = baseNamespace;		
	}
	
	@Override
	public Flux<ComparatorModel> findByBoundingBox(BoundingBox boundingBox) {
		return kvmRestEndpoint
				.findByBoundingBox(boundingBox.getLatleftTop(), boundingBox.getLngleftTop(),
						boundingBox.getLatRightBottom(), boundingBox.getLngRightBottom())
				.map(kvmEntry -> kvmEntry2PublicationComparatorModel.convert(kvmEntry, new SimpleNamespace("kvm",  baseNamespace + "kvm")));
	}

	@Override
	public String getAdapterId() {
		return "kvm";
	}
}

