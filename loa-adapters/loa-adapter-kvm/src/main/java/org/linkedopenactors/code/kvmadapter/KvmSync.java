package org.linkedopenactors.code.kvmadapter;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.util.Values;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.linkedopenactors.code.comparator.ComparatorModel;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import de.naturzukunft.rdf4j.loarepository.LastSyncDateStore;
import de.naturzukunft.rdf4j.loarepository.LoaRepositoryManager;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class KvmSync {
	
	private static final IRI SUBJECT = Values.iri("http://linkedopenactors.org/adapters/kvm");
	private KVMRestEndpoint kvmRestEndpoint;
	private KvmEntry2PublicationComparatorModel kvmEntry2PublicationComparatorModel;
	private Repository repository;
	private LastSyncDateStore lastSyncDateStore;
	private String baseNamespace;
	private String repositoryIdKvm;

	public KvmSync(LoaRepositoryManager loaRepositoryManager, @Value("${app.repositoryIdKvm}") String repositoryID,
			KVMRestEndpoint kvmRestEndpoint, KvmEntry2PublicationComparatorModel kvmEntry2PublicationComparatorModel,
			@Qualifier("KvmLastSyncDateStore") LastSyncDateStore lastSyncDateStore,
			@Value("${app.baseNamespace}") String baseNamespace,
			@Value("${app.repositoryIdKvm}") String repositoryIdKvm) {
		this.kvmRestEndpoint = kvmRestEndpoint;
		this.kvmEntry2PublicationComparatorModel = kvmEntry2PublicationComparatorModel;
		this.lastSyncDateStore = lastSyncDateStore;
		this.baseNamespace = baseNamespace;
		this.repositoryIdKvm = repositoryIdKvm;
		this.repository = loaRepositoryManager.getRepository(repositoryID).orElse(loaRepositoryManager.createRepo(repositoryID)); // TODO sollte bereits existieren, wie machen wir hier den initial load ?
	}
	
	public Mono<List<ComparatorModel>> sync() {
		try {
			Instant lastSyncDate = lastSyncDateStore.lastSyncDate(SUBJECT).orElse(ZonedDateTime.now(ZoneOffset.UTC).minusDays(5).toInstant());
			log.info("scheduler: synchronize kvm - lastSyncDate: " + lastSyncDate);
			return sync(lastSyncDate);			
		} catch (Exception e) {
			log.error("error while updateChangedKvmEntries.run() " + e.getMessage());
			return Mono.empty();
		}
	}
	
	public Mono<List<ComparatorModel>> sync(Instant lastSyncDate) throws Exception {
		Instant now = Instant.now();
		long searchForChangesInTheLastMinutes = ChronoUnit.SECONDS.between(Instant.now(), lastSyncDate);
		log.debug("-> UpdateChangedKvmEntries (look for changes in the last "+searchForChangesInTheLastMinutes+" seconds.)");
		
		return kvmRestEndpoint
				.getChangedEntriesSince(lastSyncDate.minusSeconds(10))
				.doOnNext(it->log.trace("processing: " + it))
				.map(entry->kvmEntry2PublicationComparatorModel.convert(entry, new SimpleNamespace(repositoryIdKvm, baseNamespace + repositoryIdKvm + "/")))				
				.collectList()
				.map(cm->save(cm))
				.doOnSuccess(it->{
					lastSyncDateStore.lastSync(SUBJECT, now);
					log.debug("<- UpdateChangedKvmEntries (succeeded)");
					})
				.onErrorResume(e -> {
					log.error("error while sync", e);
					return Mono.error(e);	
				});
	}

	private List<ComparatorModel> save(List<ComparatorModel> comparatorModels) {
		log.debug("save " + comparatorModels.size() + " comparatorModels.");
		Set<Statement> statements = new HashSet<>();
 		comparatorModels.forEach(cm->{
 			log.trace("adding " + cm.getSubject() + " to statements for saving.");
 			statements.addAll(cm.getModel());	
 		});
		try(RepositoryConnection con = repository.getConnection()) {
			con.add(statements);
			return comparatorModels;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} 				
	}
}
