package org.linkedopenactors.code.loaapp.controller.comperator;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"org.linkedopenactors.code"})
public class TestApplication {

//	@Bean
//	public RepositoryManager getRepositoryManager() throws MalformedURLException {
//		RepositoryManager rm = RepositoryProvider.getRepositoryManager(new File("target/repoManager"));
//		rm.init();
//		return rm;
//	}
}
