package org.linkedopenactors.code.it;

import java.io.IOException;
import java.io.StringReader;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.web.reactive.function.client.WebClient;
import org.testcontainers.containers.FixedHostPortGenericContainer;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Testcontainers;

import lombok.extern.slf4j.Slf4j;

@Testcontainers
@Slf4j
public class ITBase {
	protected static String activityPubServerUrl;
	protected static final int LOA_PORT = 8090;
	protected static Network NETWORK = Network.newNetwork();
	private static String image; 
	static {
		image = System.getProperty("image", "registry.gitlab.com/linkedopenactors/loa-suite") + ":"
				+ System.getProperty("imageVersion", "develop");
		log.info("using image: " + image);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked", "deprecation" })
	protected static GenericContainer ap = new FixedHostPortGenericContainer(image)
			.withFixedExposedPort(8090,8090)
			.withNetwork(NETWORK)
            .withExposedPorts(LOA_PORT)
			.withEnv("app.rdfRepositoryHome", "/mnt/spring/")
			.withEnv("spring_profiles_active", "it-docker")
            .withLogConsumer(new Slf4jLogConsumer(log))
            .waitingFor(Wait.forHttp("/actuator/health"));

	@BeforeAll
	public static void beforeAll() {
		activityPubServerUrl = "http://"+ap.getHost()+":"+LOA_PORT + "/";
//    	ap.withEnv("activityPupServer.url", activityPubServerUrl);
        ap.start();
	}
	
	protected Model getAllPublications(String repositoryId) throws IOException {
		String response = WebClient.builder()
				.baseUrl(activityPubServerUrl)
				.codecs(configurer -> configurer
			    		.defaultCodecs()
			    		.maxInMemorySize(16 * 1024 * 1024)
			    		)
				.build()
				.get()
				.uri("/" + repositoryId)
				.header("accept", "application/json")
				.retrieve()
				.bodyToMono(String.class)
				.block();
		Model model = Rio.parse(new StringReader(response), RDFFormat.TURTLE);
		return model;
	}

	protected Model getLoaPublication(String repositoryId, String subject) throws IOException {
		String url = repositoryId + "/" + subject;
		log.debug("getLoaPublication("+activityPubServerUrl + url +")");		
		String response = WebClient.builder()
				.baseUrl(activityPubServerUrl)
				  .build()
			    .get()
			    .uri(url)
			    .header("accept", "text/turtle")
			    .retrieve()
			    .bodyToMono(String.class)
			    .block();
		return Rio.parse(new StringReader(response), RDFFormat.TURTLE);
	}
}
