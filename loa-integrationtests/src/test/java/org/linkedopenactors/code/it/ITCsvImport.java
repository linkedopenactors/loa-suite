package org.linkedopenactors.code.it;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class ITCsvImport extends ITBase {

	@Test
	void test() throws IOException {
		String repositoryUrl = upload("BioContatti_LOA_Bolsena.csv");
		String repoId = repositoryUrl.substring(repositoryUrl.lastIndexOf("/")+1);
		log.info("repoId: " + repoId);
		Model m = getAllPublications(repoId);
		StringWriter sw = new StringWriter();
		Rio.write(m, sw, RDFFormat.TURTLE);
		log.info("model size: " + m.size());
		log.info("model: " + sw);		
		assertEquals(47, m.size());
		IRI subject = (IRI)m.stream().findAny().orElseThrow().getSubject();
		log.info("subject: " + subject);
		log.info("subject.getLocalName(): " + subject.getLocalName());
		Model pubModel = getLoaPublication(repoId, subject.getLocalName());
		log.info("### pubModel");
		sw = new StringWriter();
		Rio.write(pubModel, sw, RDFFormat.TURTLE);
		log.info("pubModel: " + sw);		
		IRI about = (IRI)pubModel.filter(subject, SCHEMA_ORG.about, null).stream().findFirst().orElseThrow().getObject();
		Model aboutModel = getLoaPublication(repoId, about.getLocalName());
		log.info("### aboutModel");
		sw = new StringWriter();
		Rio.write(aboutModel, sw, RDFFormat.TURTLE);
		log.info("aboutModel: " + sw);		

	}

	private String upload(String fileName) throws IOException {
		return WebClient.builder()
				.baseUrl(activityPubServerUrl)
				.build()
				.post()				
			    .uri("/tools/createTempRepo")
			    .accept(MediaType.TEXT_PLAIN)
			    .contentType(MediaType.MULTIPART_FORM_DATA)
			    .body(BodyInserters.fromMultipartData(getBuilder(fileName).build()))
			    .exchangeToMono(response -> {
			        if (response.statusCode().equals(HttpStatus.OK)) {
			            return response.bodyToMono(String.class);
			        } else {
			            throw new RuntimeException(response.statusCode() + " Error uploading file");
			        }
			     })
			    .block();
	}

	private MultipartBodyBuilder getBuilder(String fileName) throws IOException {
		MultipartBodyBuilder builder = new MultipartBodyBuilder();
		builder
			.part("csvFile", Files.readAllBytes(Paths.get(new ClassPathResource(fileName).getFile().getPath())))
			.header("Content-Disposition", "form-data; name=file; filename="+fileName);
		return builder;
	}
}
