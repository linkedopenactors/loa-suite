package org.linkedopenactors.code.it;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.StringWriter;
import java.time.Duration;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.WebClient;

import de.naturzukunft.rdf4j.vocabulary.SCHEMA_ORG;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class ITKvmImport extends ITBase {

	@Test
	void test() throws RDFParseException, UnsupportedRDFormatException, IOException {
		doInitialLoad();
		validatePublications();			
	}

	private void doInitialLoad() {
		WebTestClient
			.bindToServer()
			.responseTimeout(Duration.ofMillis(1200000))
			.baseUrl(activityPubServerUrl)
			.build()
			.get()
			.uri("/kvm/initialLoad")
			.exchange()
			.expectStatus().isOk();
	}

	private void validatePublications() throws RDFParseException, UnsupportedRDFormatException, IOException {
		try {
			Model creativeWorks = getAllPublications("kvm_loa").filter(null, RDF.TYPE, SCHEMA_ORG.CreativeWork);
			assertTrue(creativeWorks.size() > 23000);
			checkTeikei();			
		} catch (RDFParseException | UnsupportedRDFormatException | IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}		
	}

	public void checkTeikei() throws RDFParseException, UnsupportedRDFormatException, IOException {		
		String repositoryId = "kvm_loa";
		String subject = "V24_4a28e38695854059a457beb3b53c2578";
		Model publication = getLoaPublication(repositoryId, subject);		
		StringWriter sw = new StringWriter();
		Rio.write(publication, sw, RDFFormat.TURTLE);
		log.trace("publication: " + sw);		
		Model filtered = publication.filter(null, SCHEMA_ORG.name, null);
		assertTrue(filtered.size()==1);
		assertEquals("Teikei Gemeinschaft München-Trudering", filtered.stream().findFirst().orElseThrow().getObject().stringValue());

		// TODO
		// this is just a sample, we should use a sparql connection and parse the result bindings!
		// but before, we should get all version while initial load
		String response = WebClient.builder()
				.baseUrl(activityPubServerUrl)
				.build()
				.get()
				.uri("/kvm_loa?identifier=4a28e38695854059a457beb3b53c2578")
				.header("accept", "application/json")
				.retrieve()
				.bodyToMono(String.class)
				.block();
		log.info("checkTeikei res: " + response);

	}
}
 