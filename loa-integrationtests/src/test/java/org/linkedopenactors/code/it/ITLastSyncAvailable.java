package org.linkedopenactors.code.it;

import org.junit.jupiter.api.Test;
import org.springframework.test.web.reactive.server.WebTestClient;

class ITLastSyncAvailable extends ITBase {
	@Test
	void test() {
		WebTestClient
		  .bindToServer()
		    .baseUrl(activityPubServerUrl)
		    .build()
		    .get()
		    .uri("/lastSync")
		  .exchange()
		    .expectStatus().isOk()
		    .expectHeader().valueEquals("Content-Type", "text/html");
	}
}
