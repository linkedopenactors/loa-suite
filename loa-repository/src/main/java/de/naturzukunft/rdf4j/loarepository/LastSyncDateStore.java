package de.naturzukunft.rdf4j.loarepository;

import java.time.Instant;
import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;

public interface LastSyncDateStore {
	void lastSync(IRI subject, Instant lastSyncDate);
	Optional<Instant> lastSyncDate(IRI subject);
}
